from django.shortcuts import render

def index(request):
    return render(request, 'index.html')

def gallery(request):
    content = {'title' : gallery}
    return render(request, 'gallery.html',content)

def contact(request):
    content = {'title' : contact}
    return render(request, 'contact.html',content)

def about(request):
    content = {'title' : about}
    return render(request, 'about.html',content)