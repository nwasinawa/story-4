from django.urls import path
from django.shortcuts import render
from .views import *
from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
	path('contact', views.contact, name='contact'),
    path('about', views.about, name='about'),
	path('gallery', views.gallery, name='gallery')
]
]
